import { useState, useEffect } from 'react';
//import the navbar
import AppNavBar from './components/AppNavBar';

//acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import Catalog from './pages/Courses';
import AddCourse from './pages/AddCourse';
import ErrorPage from './pages/Error';
import LoginPage from './pages/Login';
import LogoutPage from './pages/Logout';
import CourseView from './pages/CourseView';

//acquire Provider utility in this entry point.
import { UserProvider } from './UserContext';

//implement page routing in our app
//acquire the utilities from react-router-dom.
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
//BrowserRouter -> this is a standard library component for routing in react. this enables navigation amongst views of various components. It will serve as the 'parent' component that will be used to store all the other components.
//as -> alias keyword in JS.

//Routes => it's a new component introduced in V6 of react-router-dom whose task is to allow witching between locations.

//JSX component -> self closing tag
import './App.css';

//JSX Component -> self closing tag
//syntax: <Element />
function App() {

  //the role of the Provider was assigned to the App.js, which means all the information that we will be declared here will automatically becom global.
  //Initialize a state of the user to provide/identify the status of the client who is using the app.
  
  //id => to reference the user.
  //isAdmin => role of the user
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
    }) //to bind the user to the default state.

    //feed the information stated here to the consumers.
    //Create a 'side effect that will send a request to API collection to get the identity of the user using the access token that was saved in the browser.
    useEffect(() => {
      //identify the state of user.
      console.log(user);
      //mount/campaign the user to the app so that he/she will be recognized, using the token.

      //retrieve the token from the browser storage.
      let token = localStorage.getItem('accessToken');
      console.log(token);

      //fetch('url', 'options'), keep in mind, in this request we are passing down a token.
      fetch('https://frozen-tundra-41775.herokuapp.com/users/:details', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).then(res => res.json()).then(convertedData => {
        console.log(convertedData)
      });


    },[]);

  return (
    <UserProvider value={{user, setUser}}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route path ='/' element={<Home />} />
          <Route path ='/Register' element={<Register />} />
          <Route path ='/courses' element={<Catalog />}/>
          <Route path ='/add-course' element={<AddCourse />}/>
          <Route path ='/login' element={<LoginPage />}/>
          <Route path ='/logout' element={<LogoutPage />}/>
          <Route path ='/courses/view-course/:id' element={<CourseView />}/>
          <Route path ='*' element={<ErrorPage />}/>
        </Routes>
      </Router>
    </UserProvider>
  );
};

export default App;
