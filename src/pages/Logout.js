//Identify the components that will use for this page
import Hero from './../components/Banner';
const data = {
	title: 'You have Logged out',
	content: 'Sign in to create a new session'
}

//create a function that will describe the srtucture of the page.
export default function Logout() {


	return(
		<Hero bannerData={data} />
	);
};