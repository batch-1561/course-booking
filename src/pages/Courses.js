//Identify which components will be displaed
import { useState, useEffect } from 'react'; //•8
import Hero from '../components/Banner';
import CourseCard from './../components/CourseCard'
import { Container } from 'react-bootstrap' //•7

const bannerDetails = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of Courses'

};

//Catalog all Active Courses
	//1. Need Container Components for each course that we will retrieve from the database
	//2. Declare state for the courses collection by default it is an empty array.
	//3. •10 Create a side effect that will send a request to our backend api project


export default function Courses() {
	//•9 this array/storage will be used to save our course components for all active courses.
	const [coursesCollection, setCourseCollection] = useState([]);

	// •11 create am effect using our effect Hook, provide a list of dependencies.
	//•12 the effect that we are going to create is fetching data from the server.
	useEffect(() => {
		//•13 fetch() => is a JS method which allows us to pass/ create a request to an API.
		// •14  SYNTAX: fetch(<request URL>, {OPTIONS})
		//•15 GET HTTP METHOD => NO NEED TO INSERT OPTIONS.

		//•16remember that once you send a request to an endpoint, a 'promise' is returned as a result.
		//•17 A promise has 3 states: (fullfilled, reject, pending)
		//•18 we need to be able to handle the outcome of the promise.
		// •19 we need to make the response usable on the frontend side. we need to convert it to a json format.
		// •20 upon converting the fetched data to a json format a new promise will be executed.
		//•21-•22 in S29-s3
		//•23 upon converting the fetched data to a json format a new promise will be executed
		fetch('https://frozen-tundra-41775.herokuapp.com/courses/')
		.then(res => res.json())
		.then(convertedData => {
			console.log(convertedData);
			//•24 we want the resources to be displayed in the page
			//•25 since the data is stored in an array storage, we will iterate the array to explicitly get each document one by one.
			//•26 there will be multiple course card components that will be rendered that explicitly get each document one by one.
			// •27  there will be multiple course card component that will be rendered that corresponds to each document retrieved from the database. we need to utilize a storage to contain the cards that will be rendered.
			//•28 key attribtute => as a reference to avoid rendering duplicates of the same element/object.

			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
					)
			})) 
		});
	},[]);

	return(
		<>
			<Hero bannerData={bannerDetails} />
			<Container>
				{coursesCollection}
			</Container>
			
		</>
	);
};