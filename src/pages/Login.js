//identify the components that will used for this page

import { useState, useEffect } from 'react';

import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2'; 

const data = {
  title: 'Welcome to Login',
  content: 'Sign in your account below'
}
//create a function that will describe the structure of the page. 
export default function Login() {

	//declare an initial/default state for our form elements.
	//Bind/Lock the form elements to the desired states.
	//Assign the states to their respective components.
	//SYNTAX: const/ let [getter, setter] = useState()
	const [email, setEmail ] = useState(''); 
	const [password, setPassword ] = useState('');

	//what validation are we going run on the email.
	//(format: @, dns)
	//search a character within a string
	let addressSign = email.search('@');
	//TRIVIA: if the search() fins No match inside the string it will return -1.
	//tip: if youre going to pasown multiple values on the query. contain it inside an array. "includes()".
	let dns = email.search('.com') //multiple must have another line.


	//declare a state for the login button
	const [isActive, setIsActive] = useState(false);
	//apply a conditional rendering to the button component for its current state
	const [isValid, setIsValid] = useState(false);

	//create a side effect that will make our page 'reactive'.
	useEffect(() => {
		//Create a logic/ condition that will evaluate the format of the email.
		//if the value is -1 (no match found inside the string)
		if (dns !== -1 && addressSign !== -1) {
				setIsValid(true);
				if (password !== '') {
					setIsActive(true);
				} else {
					setIsActive(false);
				}
		} else {
				setIsValid(false);
		}
	},[email, password, addressSign, dns])

	//Create a simulation that will help us identify the workflow of the login page. 
	const loginUser = async (event) => {
		event.preventDefault();

		//send a request to verify if the user's identity is true.
		//syntax: fetch('url', {options})
		fetch('https://frozen-tundra-41775.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
    			"email": email,
    			"password": password
    
			})
		}).then(res => res.json()).then(dataNaJson => {
				let token = dataNaJson.accessToken;
				// console.log(token); //who?

				//create a control structure to give a proper response to the user
				if (typeof token !== 'undefined') {
						//prompt ng message kay user
						
						//The produced token, save it on the browser storage. (storage area)
						//to save an item in the localStorage object of the browser... user setItem(key, value)
										//key, value
						localStorage.setItem('accessToken',token);

						Swal.fire({
							icon: 'success',
							title: 'Login Successful',
							text: 'Welcome!'
						})
				} else {
						Swal.fire({
							icon: 'error',
							title: 'Check your Credentials',
							text: 'Contact Admin if problem persist!'
						})
				}
		})

	};


		// //[milestone Check]

		// //Create a demo video for the following
		// 	=> Course Catalog
		// 			(Properly Formatted by 3s)
		// 	=> Display Single Course
		// 	=> Registration with Reactive Components
		// 	=> Login with token in localStorage
		// 2 demons atleast




	return(
	   <>
			<Hero bannerData={data} />

			<Container>
				<h1 className="text-center">Login Form </h1>
				<Form onSubmit={e => loginUser(e)}>
				    {/*Email Address Field*/}
					<Form.Group>
						<Form.Label>Email: </Form.Label>
						<Form.Control 
							type="email"
							placeholder="Enter Email here.."
							required
							value={email}
							onChange={event => {setEmail(event.target.value)} }
						/>

						{
							isValid ?
								<h6 className="text-success"> Email is Valid </h6>
							:
								<h6 className="text-muted"> Email is Invalid </h6>
						}

					</Form.Group>

					{/*Password Field*/}
					<Form.Group>
						<Form.Label>Password: </Form.Label>
						<Form.Control 
							type="password"
							placeholder="Enter Password here.."
							required
							value={password}
							onChange={event => {setPassword(event.target.value)} }
						/>
					</Form.Group>

					{
						isActive ?
						<Button
					  	className="btn-block" 
					  	variant="success"
					  	type="submit"
						>Login
						</Button>
						:
						<Button
					  	className="btn-block" 
					  	variant="secondary"
					  	disabled
						>
						Login
						</Button>
					}

				</Form>
			</Container>

		</>
	);
};
