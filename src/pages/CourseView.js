//this will serve as page whenever the client would want to select a single course/item from the catalog
import { useState, useEffect } from 'react';
import Hero from './../components/Banner';
//Grid System, Card, Buttom
import {Row, Col, Card, Button, Container} from 'react-bootstrap';
//•30 declare a state for the course details. we need to use the correct 'hook'.

//import sweetalert
import Swal from 'sweetalert2';

//routing component //•33 add use params
import { Link, useParams } from 'react-router-dom';

const data = {
	title: 'Welcome to B156 Booking-App',
	content: 'Check out our school campus'
}

export default function CourseView() {

	//state of our course details
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	//•31 id that will be used as a reference to determine which course will be displayed by the page. acquire a 'Hook' which will allow us to manage/access the data in the browser URL.

	//•32 using the parameter Hook(useParams) -> this will be provided by the 'react-router-dom'

	//•34 the useParams will return an object that will contain the path variables stored in the URL.
	//•35 take a peek at the data inside the URL
	console.log(useParams());
	//•36 observe, it should contain an object that stores the id of the targeted course.

	//•37 extract the value from the path variables by destructuring the object.
	const {id} = useParams()
	console.log(id);

	//•38 Create a *side effect* which will send a request to our backend API for course-booking. use the proper 'hook'(effect hook).

	useEffect(() => {

		//•39 send a rquest to our API to retrieve information about the course.
		//syntax: fetch(<URL ADDRESS>','<>)
		fetch(`https://frozen-tundra-41775.herokuapp.com/courses/${id}`).then(res => res.json())
		.then(convertedData => {
			/*console.log(convertedData);*/
			//•40 the data that we retrieved from the db, we need to contain.
			//•41 call the setter to change the state of the course info for this page

			setCourseInfo({
				name: convertedData.name, 
				description: convertedData.description,
				price: convertedData.price
			})
		});
	}, [id])

	const enroll = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'Enrolled Succesfully!',
				text: 'Thank you for enrolling to this course.'
			})
		);
	};


	return(
		<>
			<Hero bannerData={data} />
			<Row>
				<Col>
					<Container>
						<Card className="text-center">
							<Card.Body>
								{/*<!-- Course Name -->*/}
								<Card.Title>
								<Card.Subtitle>
									<h2> {courseInfo.name} </h2>
								</Card.Subtitle>
								</Card.Title>
								{/*<!-- Course Description -->*/}
								<Card.Title>
								<Card.Subtitle>
									<h5 className="my-4"> Description </h5>
								</Card.Subtitle>
								<Card.Text>
									{courseInfo.description}
								</Card.Text>
								</Card.Title>
								<Card.Subtitle>
									<h5 className="my-4"> Price: {courseInfo.price} </h5>
								</Card.Subtitle>
							</Card.Body>
							<Button variant="warning" className="btn-block" onClick={enroll}>
							Enroll
							</Button>
							<Link className="btn-block btn-success mb-5" to="/login">
								Login to Enroll
							</Link>
						</Card>
					</Container>
				</Col>
			</Row>
		</>
	);
};