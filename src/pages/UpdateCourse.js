import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Update Course Page',
	content: 'Update Course details below.'
}

export default function Update() {

	const updateCourse = (eventSubmit) => {
		event.Submit.preventDefault()
		return(
			Swal.fire({
				icon: 'success',
				title: 'The course has been created successfully!',
				text: 'Thank you!'
			})
		);
	};

	return
		<div>
			<Hero bannerData={data} />
			<Container>
				<h1 ClassName="text-center">Update Form</h1>
				<Form onSubmit={e => updateCourse(e)}>
					<Form.Group>
				</Form>
			</Hero>
		</div>
};