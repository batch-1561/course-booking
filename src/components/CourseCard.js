//Identify needed components
import {Card} from 'react-bootstrap';

//•29 Element with routing capability
import { Link } from 'react-router-dom';
//•30 we are holding

export default function CourseCard({courseProp}) {
	return(
		<Card>
			<Card.Body>
				<Card.Title>
					{courseProp.name}
				</Card.Title>
				<Card.Text>
					{courseProp.description}
				</Card.Text>
				<Card.Text>
					Price: {courseProp.price}
				</Card.Text>
				<Link to={`view-course/${courseProp._id}`} className="btn btn-primary">
					View Course
				</Link>		
			</Card.Body>
		</Card>
		
		)
}
