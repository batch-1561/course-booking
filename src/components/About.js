//[ACTIVITY]

//Identify needed components
import {Card,} from 'react-bootstrap';

export default function AboutMeCard() {
	return(
		<Card className="myNameCard">
			<Card.Body>
				<Card.Title className="pb-5">
					About Me
				</Card.Title>
				<Card.Text className="dyCabreros">
					Dylan Cabreros
				</Card.Text>
				<Card.Text>
					Full Stack Web Developer
				</Card.Text>
				<Card.Text>
					Started my Full Stack Web Developer job with the help of Zuitt Coding Bootcamp.
				</Card.Text>
				<Card.Text>
					Contact me
				</Card.Text>
				<Card.Text>
					<ul>
						<li>Email: dcabreros@google.com</li>
						<li>Mobile No: 09696969696</li>
						<li>Address: Ponte Fino, Batangas City</li>
					</ul>
				</Card.Text>
			</Card.Body>
		</Card>
		)
}