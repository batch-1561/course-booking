//Identify which components are needed to build the navigation
import { Navbar, Nav, Container } from 'react-bootstrap'

//implement Links in the navbar
import { Link } from 'react-router-dom';
//import sweetalert
import Swal from 'sweetalert2';


//We will now describe how we want our Navbar to look.
function AppNavBar() {

	const logout = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'Logged out Succesfully!',
				text: 'Log in to create session.'
			})
		);
	};


	return(
			<Navbar bg="primary" expand="lg">
				<Container>
					<Navbar.Brand> B156 Booking App </Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse>
					<Nav className="ml-auto">
						<Link to="/" className="nav-link text-light">
							Home
						</Link>
						<Link to="/register" className="nav-link text-light">
							Register
						</Link>
						<Link to="/courses" className="nav-link text-light">
							Courses
						</Link>
						<Link to="/add-course" className="nav-link text-light">
							Add Course
						</Link>
						<Link to="/login" className="nav-link text-light">
							Login
						</Link>	
						<Link to="/logout" className="nav-link text-light" onClick={logout}>
							Logout
						</Link>				
					</Nav>
				</Navbar.Collapse>
				</Container>
									
			</Navbar>	
		);
};


export default AppNavBar;